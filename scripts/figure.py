'''
Author:       Adam Conn
Date Created:
Institution: The Salk Institute of Biological Science-Integrative Biology Laboratory - Saket Navlakha's Laboratory

Purpose: This is the main figure file. It will be inherited by all figures
'''
import numpy as np
from random import randint
from scipy.stats import linregress
from math import log10
import matplotlib.pyplot as plt
from glob import glob
from datetime import datetime


class Figure(object):

    def __init__( self, scale=1, KMax=21, rotate=True, segments=1, filters=[] ):
        self.helper = Helper( scale=scale, KMax=KMax, rotate=rotate, segments=segments )
        self.filters = filters

        self.fontsize=20
        self.lfontsize=12
        self.fontweight='bold'
        self.lw=3




    def retrieveJSON(self):
        pass

    def plotFigure(self):
        pass

    def update_parameters( self, **kwargs):
        args = ['scale', 'KMax', 'segments', 'rotate', 'filters']

        for key in kwargs:
            if key not in args:
                raise ValueError( "\n{} is not a valid keyword in update_paramters: ".format(key) /
                                    "valid options are scale, KMax, segments, filters")
            else:
                if key == "filters":
                    self.filters = kwargs[key]
                else:
                    self.helper.__dict__[key] = kwargs[key]

    def get_parameters(self):
        return {
            'scale':     self.helper.scale,
            'segments':  self.helper.segments,
            'KMax':      self.helper.KMax,
            'filters':   self.filters,
        }

    def log_parameters( self, figname ):
        params = self.get_parameters()
        time = str(datetime.now())
        new_entry = '\n'
        new_entry += time + '\n'
        new_entry += figname + '\n'
        for key in params:
            new_entry += key + ': ' + str(params[key]) + '\n'

        with open( './../figures/fig_parameter_log.txt', 'a') as f:
            f.write( new_entry )




"""
A class to define the different parameters the graphs use and a few functions
"""



#each figure object will have a helper that stores variables and miscilaneous functions
class Helper():
    def __init__(self, scale=1, KMax=21, rotate=True, segments=1):
        self.scale = scale
        self.KMax = KMax
        self.rotate = rotate
        self.segments = segments

    # return a boilerplate figure
    def custom_fig(self):
        fig = plt.figure()
        plt.style.use('ggplot')
        plt.rc('font', **{'sans-serif': 'Arial',
         'family': 'sans-serif'})
        ax = plt.gca()
        ax.tick_params(axis='both', which='major', labelsize=20)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')



        return fig, ax


    #return euclidean distance
    def euclidean_distance( self, p2, p1 ):
        return float( sum( [ (p2i - p1i)**2 for p2i,p1i in zip(p2, p1)]))**.5

    #return points,
    # n is number to divide length into, and returns midpoints between the lengths
    def interval_points( self, n, p2, p1 ):
        points =  []   #points should be length of n
        weights = []
        l = float(self.euclidean_distance(p2, p1)) / n #subdivide length into n regions

        #for each subdivision, find a midpoint within it
        for i in range( n ):
            new_point = []
            weights.append(l) #append length

            # get point for each coordinate, scale p2[i]-p1[i] to match the subdivision it is
            for p2i, p1i in zip(p2, p1):
                pi = (2*i+1) * ((p2i-p1i)/(2*n)) + p1i
                new_point.append(pi)
            points.append(new_point)
        return weights, points




    def rotate_points( self, weights, Points , printl=False):
        '''Return the rotated data'''
        Points = Points - np.mean(Points, axis=0) #subtract the mean

        covariance = self.compute_cov(weights,Points)  #get the covariacne

        Eigenvals, EigenVecs = np.linalg.eig( covariance )  #get the eigenvectors for the covariance matrix

        '''rotate the points to new axis'''
        Points = np.dot( Points, EigenVecs )

        #added in an attempt to shift values beyond 0 - resulted in no difference however was carried through
        # in the analysis for consistency 
        Points += 100


        return Points


    #input: Matrix of pOints: assumes mean is subtracted
    #ouput: 3x3 covariance matrix
    def compute_cov(self, w, Points):
        '''Computes the co-variance matrix of the data'''


        x = Points[:,0]
        y = Points[:,1]
        z = Points[:,2]

        #compute the diaganol of covariance
        xx = np.sum( w * x * x, axis=0)
        yy = np.sum( w * y * y, axis=0)
        zz = np.sum( w * z * z, axis=0)

        #compute the non-diagonal of covariance
        xy = np.sum( w * x * y, axis=0)
        xz = np.sum( w * x * z, axis=0)
        yz = np.sum( w * y * z, axis=0)

        return np.asarray( [[xx, xy, xz], [xy, yy, yz], [xz, yz, zz]])



    '''
    Input: a value of k and matrix of points belonging to single plant - (assumes values are centered (and rotated if desired))
    Output: x and y value related to figure 2B of Arbor Moments
    '''
    def xy_arbor_moment( self, k,  weights, Points ):

        '''Take a matrix of points and return '''
        x_moment = 0
        y_moment = 0  #values to return
        x_moment_normal = 0

        w = list(weights)
        x = list(Points[:,0])
        y = list(Points[:,1])
        z = list(Points[:,2])
        ux = sum(x) / len(x)
        uy = sum(y) / len(y)
        uz = sum(z) / len(z)

        xsum, ysum, zsum = [0, 0, 0]
        for wi, xi, yi, zi in zip(w, x, y, z):
            xsum += abs(wi * (xi-ux)**2)
            ysum += abs(wi * (yi-uy)**2)
            zsum += abs(wi * (zi-uz)**2)
        x_moment = ( xsum * ysum * zsum)**.5 / (sum(w)**1.5)

        for i in range(len(x)):
            y_moment += abs(((x[i]-ux)**k * (y[i]-uy)**k * (z[i]-uz)**k * w[i]))



        #return log10 moment
        try:
            x_moment = log10(x_moment)
            y_moment = log10(y_moment)

        except:
            print('in except')
            return -1, -1


        return x_moment, y_moment


    '''
    Input: A value K and a matrix of points representing a single plant
    Output: X and Y value related to figure 1B of chucks paper
    '''
    def xy_separability(self, k, weights, Points ):

        '''Take a matrix of points and return '''
        x_moment = 0
        y_moment = 0  #values to return

        w = list(weights)
        x = list(Points[:,0])
        y = list(Points[:,1])
        z = list(Points[:,2])
        ux = sum(x) / len(x)
        uy = sum(y) / len(y)
        uz = sum(z) / len(z)


        xsum, ysum, zsum = [0, 0, 0]
        for wi, xi, yi, zi in zip(w, x, y, z):
            xsum += abs(wi * (xi-ux)**k)
            ysum += abs(wi * (yi-uy)**k)
            zsum += abs(wi * (zi-uz)**k)



        x_moment = (xsum * ysum * zsum) / (sum(w))**2


        #Points should be centered prior- get number of points, square elementwise, sum columns, multiply sums, and normalize with n^3.
        #Finally, take log for x moment
        ''' Get y moment
            Sum( xi^k * yi^k * zi^k )
        '''
            # Second, compute yval.

        for i in range(len(x)):
            y_moment += (abs((x[i]-ux)**k) * abs((y[i]-uy)**k) * abs((z[i]-uz)**k * w[i]))


        try:
            #return log10 moment
            x_moment = log10(x_moment)
            y_moment = log10(y_moment)
        except:
            print('log(-n) value throws an error')

        return x_moment, y_moment


    '''
    Parse the chuck point files:
    Input: filename, rotate=True
            filename: chuckpoint file to parse
            rotate: whether to center and rotate the data
    Returns:
        weights = 1D array - index i has length associated with point at index i of Points
        Points = n x 3 matrix - point per row
    '''
    def parse_cp( self, fname, rotate=True):

        # Changed code to remove root of sorghum to be consistent with other plants
        # Sorghum highlights later days have secondary shoots that were not considered
        # in analysis due to intractability
        is_sorg = False


        with open( fname, 'r') as f:
            coordinates = {}
            neighbors = {}
            in_edges = False
            is_root = False
            for line in f:

                if line.strip() == "": continue # blank lines.
                if line.startswith("#end"): continue
                if line.startswith("#edges"):
                    in_edges = True
                    continue
                if line.startswith("#SA"): break #don't collect surface areas

                if not in_edges: # parse coordinates.

                    # Get rid of quotes, if there.
                    line = line.replace("\"","")

                    # Split by "," to get the coordinates.
                    cols = line.strip().split(",")

                    if len(cols) != 4:
                        print(file_name)
                        raise ValueError("Something wrong with this line (> 4 columns): %s" %(line))
                    x,y,z = map(float,cols[1:])


                    # Extract point and label.
                    point = cols[0].split(" ")[0] # "r -nom-"" -> "r"

                    #check to see if it is a root and don't remove for sorghum
                    if point.startswith('r') and not is_sorg:
                        continue

                    coordinates[point] = [self.scale*x,self.scale*y,self.scale*z]

                else: # parse edges.
                    point, edges = line.split(':') # r: s1, s2

                    #add neighbors to list
                    nodes = []
                    for node in edges.split(','):
                        nodes.append(node.strip())

                    #check to make sure you don't add neighbors of root
                    if point.startswith('r') and not is_sorg:
                        continue

                    #have "point" point to its neighbors
                    neighbors[point] = nodes

        # used dicts to return a vector of form
        # note, may divide line into multiple segments to compare results
        # v = [l1, x1, y1, z1]
        #     [l2, x2, y2, z2]
        #     ...etc
        # where x y z are the midpoints
        weights, Points = [], []
        for key in neighbors:
            for node in neighbors[key]: #each neighbor
                w, p = self.interval_points( self.segments, coordinates[key], coordinates[node])
                for i in range(len(w)):
                    weights.append(w[i])
                    Points.append(p[i])
        weights = np.array(weights)
        Points = np.array(Points)

        if self.rotate:
            Points = self.rotate_points(weights, Points)


        return weights, Points

    # if day of filename is less than or equal to day, False is returned
    def filt( self, fname, strain='', env='', day='' ):

        #first plant is above day threshold, if not, no need to continue and return false
        if day:
            #print('changed WTF the way you split the day for linux split()' )
            fday = fname.split('\\')[-1].split('_')[-1].split('.')[0][1:]
            fday = int(fday)
            if fday <= int(day):
                return False

        #check if strain is in fname, if not return false
        if strain:
            if strain not in fname: return False
        if env:
            if env not in fname: return False

        #if its made it here return true


        return True


        #bootstrap function
    #boot strap error as defined by a confidence interval and selected by percentile
    #hard coded to %99 confidence
    def bs_err3(self,  m, x, y, iterations=1000):
        #conf is the confidence interval, get the percentile indices to obtain

        loweri = int(iterations * (1 - float(99)/100))
        upperi = int( iterations * float((99)/100))



        n = len(x)
        slopes = []

        for i in range(iterations):
            new_x = []
            new_y = []
            for j in range(n):
                r = randint(0, n-1)
                new_x.append( x[r] )
                new_y.append( y[r] )
            mr, br, r, p, e = linregress(new_x, new_y)
            slopes.append( mr - m )


        slopes = sorted(slopes) #sort the slopes and find the values at 2.5 and 97.5 confidence



        lower_m = slopes[loweri]
        upper_m = slopes[upperi]


        return lower_m, upper_m

    def _print_moment_table(self):
        outfile = open( 'moment_table.txt','w')
        outfile.write( '{:40}'.format('plant'))
        outfile.write( '{:5}'.format('k'))
        outfile.write( '{:10}'.format('m_kx'))
        outfile.write( '{:10}'.format('m_ky'))
        outfile.write( '{:10}'.format('m_kz'))
        outfile.write( '{:10}'.format('mk'))
        outfile.write( '{:10}'.format('m0'))
        outfile.write('\n')

        i = 0
        for fname in glob( '../../chucks_points/*.cp.txt'):


            weights, Points = self.parse_cp( fname, rotate=self.rotate )
            fname = fname.split('/')[-1]

            for k in range(20):
                ki, xi, yi, zi, mk, mo = self._separate_moment(k, weights, Points)
                outfile.write( '{:<40}'.format(fname))
                outfile.write( '{:<5}'.format(ki))
                outfile.write( '{:<10}'.format(xi))
                outfile.write( '{:<10}'.format(yi))
                outfile.write( '{:<10}'.format(zi))
                outfile.write( '{:<10}'.format(mk))
                outfile.write( '{:<10}'.format(mo))
                outfile.write('\n')




        outfile.close()

    def _separate_moment(self, k, weights, Points ):
        '''Take a matrix of points and return '''
        x_moment = 0
        y_moment = 0  #values to return
        x_moment_normal = 0


        w = list(weights)
        mo = str(log10(sum(w)))
        x = list(Points[:,0])
        y = list(Points[:,1])
        z = list(Points[:,2])
        ux = sum(x) / len(x)
        uy = sum(y) / len(y)
        uz = sum(z) / len(z)


        xsum, ysum, zsum = [0, 0, 0]
        for wi, xi, yi, zi in zip(w, x, y, z):

            xsum += abs(wi * (xi-ux)**k)
            ysum += abs(wi * (yi-uy)**k)
            zsum += abs(wi * (zi-uz)**k)



        for i in range(len(x)):
            y_moment += abs(((x[i]-ux)**k * (y[i]-uy)**k * (z[i]-uz)**k * w[i]))



        #return log10 moment
        try:
            xsum = '{:.3f}'.format(log10(xsum))
        except:
            xsum = "NaN"
                #return log10 moment
        try:
            ysum = '{:.3f}'.format(log10(ysum))
        except:
            ysum = "NaN"
        try:
            zsum = '{:.3f}'.format(log10(zsum))
        except:
            zsum = "NaN"
                #return log10 moment
        try:
            mk = '{:.3f}'.format(log10(y_moment))
        except:
            mk = "NaN"

        k = str(k)


        return [k, xsum, ysum, zsum, mk, mo]
