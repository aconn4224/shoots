from similarity import *
import matplotlib


class OddEvenSimilarityScatter( SimilarityScatter ):
    def __init__(self):
        SimilarityScatter.__init__(self)

    def plotFigure(self, kstep=2, saveTo=""):

        Axis = self.build_axis( kstep=kstep)


        colors = cm.rainbow(np.linspace(0, 1, len(Axis)))
        ck = -1
        fig = self.helper.custom_fig()
        plt.figure( figsize=(8, 8))

        matplotlib.rc('xtick', labelsize=20)
        matplotlib.rc('ytick', labelsize=20)

        for k in sorted(Axis.keys()):
            ck += 1
            x_axis, y_axis = Axis[k]
            m, b, r, p, std_err = linregress(x_axis, y_axis)

            #remove zero values from axis
            for i in reversed( range(len(x_axis))):
                if x_axis[i] < 0 or y_axis[i] < 0:
                    del x_axis[i]
                    del y_axis[i]

            line = [ (m*xi + b) for xi in x_axis]

            plt.scatter( x_axis, y_axis, color=colors[ck], label='k=' + str(k))
            plt.plot( x_axis, line, color=colors[ck], lw=self.lw )


            plt.xlabel('log' + '$\\mathregular{(\\sigma_{xyz})}$', fontsize=1.2*self.fontsize, fontweight="normal")
            plt.ylabel('log' + '$\\mathregular{(\\frac{m_k}{m_0})}$', fontsize=1.2*self.fontsize, fontweight="normal")
            ax = plt.gca()
            ax.set_xlim([-.5, 12])
            ax.set_ylim([-10, 250])
            plt.tight_layout()


        if saveTo:

            saveTo = "./../figures/other_figs/" + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'similarity odd and even')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()





class OddEvenSimilaritySlopes( OddEvenSimilarityScatter ):
    def __init__(self):
        OddEvenSimilarityScatter.__init__(self)

    def build_axis(self, iterations=1, kstep=2):
        Axis = super( OddEvenSimilaritySlopes, self).build_axis(kstep=kstep)

        ks = []
        slopes = []
        m_errors = []
        for k in sorted(Axis.keys()):
            ks.append(k)
            x_axis, y_axis = Axis[k]
            m, b, r, p, e = linregress( x_axis, y_axis)
            slopes.append( m )

            m_error = []
            for i in range(iterations):
                new_i = [randint(0, len(x_axis) - 1) for i in range(len(x_axis))]
                new_x = [x_axis[i] for i in new_i]
                new_y = [y_axis[i] for i in new_i]

                m1, b1, r, p, e = linregress(new_x, new_y)

                m_error.append( m - m1)

            # part 1 and 2
            loweri = 0
            upperi = len(m_error) - 1

            m_error = sorted(m_error)
            lower = m_error[loweri]
            upper = m_error[upperi]
            m_error = max(abs(lower), abs(upper))
            m_errors.append(m_error)
        return ks, slopes, m_errors

    def plotFigure(self, iterations=1, saveTo='', kstep=2):

        ks, slopes, m_errors = self.build_axis(iterations=iterations, kstep=kstep )
        m, b, r, p, m_error = linregress(ks, slopes)

        fig = self.helper.custom_fig()

        #bootstrap error is in equation
        #eqt = 'slope = ' + '{0:.3f}'.format(m) + '$\pm$' + '{:.3f}'.format(m_error)
        eqt = 'slope = ' + '{0:.3f}'.format(m)

        #errorbars come from std error
        plt.errorbar(ks, slopes, yerr=m_errors, marker='o', c='#f44e42', markeredgecolor='#f44e42', mew=2, ms=12, markerfacecolor='none',  lw=3, label=eqt)
        plt.legend( loc='upper left', fontsize=15, numpoints=1)
        plt.xlabel('Moment order (k)', fontsize=self.fontsize)
        plt.ylabel('Slope', fontsize=self.fontsize)

        ax = plt.gca()
        start, endx = ax.get_xlim()
        starty, endy = ax.get_ylim()
        end = max(endx, endy)

        ax.xaxis.set_ticks(np.arange(0, 21, 5))
        ax.yaxis.set_ticks(np.arange(0, 21 , 5))
        ax.set_xlim(-.5, 21)
        ax.set_ylim(-.5, 21)

        #ax.set_xlim( [0, 20])
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/other_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'slopes vs k similarity')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()







class OddEvenSimilarityIntercepts( OddEvenSimilarityScatter ):
    def __init__(self):
        OddEvenSimilarityScatter.__init__(self)

    def build_axis(self, iterations=1, kstep=2):
        Axis = super( OddEvenSimilarityIntercepts, self).build_axis(kstep=kstep)

        ks = []
        intercepts = []
        b_errors = []
        for k in sorted(Axis.keys()):

            ks.append(k)
            x_axis, y_axis = Axis[k]
            m, b, r, p, e = linregress( x_axis, y_axis)
            intercepts.append( b )

            b_error = []
            for i in range(iterations):
                new_i = [randint(0, len(x_axis) - 1) for i in range(len(x_axis))]
                new_x = [x_axis[i] for i in new_i]
                new_y = [y_axis[i] for i in new_i]

                m1, b1, r, p, e = linregress(new_x, new_y)

                b_error.append( b - b1)

            # part 1 and 2
            loweri = 0
            upperi = len(b_error) - 1

            b_error = sorted(b_error)
            lower = b_error[loweri]
            upper = b_error[upperi]
            b_error = max(abs(lower), abs(upper))
            b_errors.append(b_error)
        return ks, intercepts, b_errors

    def plotFigure(self, iterations=1, kstep=2, saveTo="", distributions=[]):

        ks, intercepts, b_errors = self.build_axis( iterations=iterations, kstep=kstep)
        m, b, r, p, std_err = linregress(ks, intercepts)

        fig = self.helper.custom_fig()

        yg = self._getGaussians( )

        plt.errorbar(ks, intercepts, yerr=b_errors, marker='o', color='red', lw=3, label="Plants: ")
        #plt.plot(yg['x'],yg['yg14'],lw=3,c="black", linestyle='--',label="Gaussian 1.4")
        #plt.plot(yg['x'],yg['yg24'],lw=3,c="black", linestyle='--', label="Gaussian 2.4")
        #plt.plot(x,yg26,lw=3,c="black", linestyle='--', label="Gaussian 2.6")
        plt.plot(yg['x'],yg['yg16'],lw=3,c="black", linestyle='-',label="Gaussian 1.6")
        plt.plot(yg['x'],yg['yg34'],lw=3,c="black", linestyle='--',  label="Gaussian 3.4")

        plt.plot(yg['x'],yg['yp'],lw=3,c="black", linestyle='-.',  label="Uniform")
        plt.legend(loc=2)
        plt.xlabel('Moment order (k)', fontsize=self.fontsize)
        plt.ylabel('Intercept', fontsize=self.fontsize)

        ax = plt.gca()

        handles, labels = ax.get_legend_handles_labels()
        ax.legend( [handles[0],handles[1], handles[3], handles[2]],  [labels[0],labels[1], labels[3], labels[2]], loc='upper left', fontsize=self.lfontsize, numpoints=1)

        start, endx = ax.get_xlim()
        starty, endy = ax.get_ylim()
        end = max(endx, endy)

        ax.xaxis.set_ticks(np.arange(0, 21, 5))
        ax.yaxis.set_ticks(np.arange(0, 21 , 5))
        ax.set_xlim(0, 21)
        ax.set_ylim(0, 21)

        #ax.set_xlim( [0, 20])
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/similarity_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'slopes vs k similarity')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()


    def _getGaussians( self ):

        yg = {}
        '''Sakets plots for different distributions'''
        add = lambda q: q + 1

        # plant.
        yg['x'] = [0,2,4,6,8,10,12,14,16,18,20]

        #pillbox.
        yg['yp'] = list(map(add,[0.0,0.0,0.597317,1.36065,2.20276,3.09106,4.00975,4.94996,5.90621,6.87489,7.85348]))

        #gaussian 1.4
        yg['yg14'] = list(map(add,[0.0,0.0,0.670731,1.52291,2.45979,3.4457,4.46358,5.50395,6.56099,7.6309,8.71103]))

        #gaussian 1.6
        yg['yg16'] = list(map(add,[0.0,0.0,0.696,1.578,2.548,3.567,4.619,5.693,6.785,7.889,9.004]))

        #gaussian 2.4
        yg['yg24'] = list(map(add,[0.0,0.0,0.844311,1.91191,3.07934,4.30278,5.56195,6.84586,8.14788,9.46375,10.7905]))

        #gaussian 2.6
        yg['yg26'] = list(map(add,[0.0,0.0,0.894,2.026,3.261,4.556,5.887,7.244,8.619,10.009,11.409]))

        #gaussian 3.4
        yg['yg34'] = list(map(add,[0.0,0.0,1.12437,2.56915,4.14974,5.80229,7.49877,9.22468,10.9716,12.7344,14.5094]))

        #gaussian 3.6
        yg['yg36'] = list(map(add,[0.0,0.0,1.182,2.712,4.391,6.146,7.948,9.781,11.635,13.506,15.390]))

        return yg
