import matplotlib.pyplot as plt

plt.style.use('ggplot')
plt.rc('font', **{'sans-serif': 'Arial',
 'family': 'sans-serif'})
fig = plt.figure()

x = [i for i in range(10)]
y = [xi**2 for xi in x]

xlabel = '$\\mathdefault{\\log(gm_{2} / m_{0})}$'
ylabel = 'log(' + '$\\mathregular{m_{2} / m_{0}}$)'
plt.xlabel(xlabel, fontsize=22, fontweight="normal")
plt.ylabel(ylabel, fontsize=22, fontweight="normal")
plt.plot(x, y)
plt.tight_layout()
plt.show()
