from figure import Figure
from glob import glob
from scipy.stats import linregress
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

class SeparabilityScatter( Figure ):
    def __init__(self):
        Figure.__init__(self)




    #returned teh moments, other processing in axis will take place in the graph
    def build_axis( self ):
        x_axis = []
        y_axis = []
        #iterate through the files and build the axis points
        for fname in glob( './../data/*.cp.txt' ):
            fname_processed = fname.strip( '.cp.txt' ).split('/')[-1]

            #filter out the plants to exclude
            if not self.helper.filt( fname_processed, *self.filters ):
                continue


            '''
            ############################################################################
            ############################################################################
            #  OVERWRITE THIS BLOCK TO ACCOMODATE POINT CLOUD
            #
            ############################################################################
            '''
            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )

            for k in range(0, self.helper.KMax, 2 ):
                x, y = self.helper.xy_separability( k, weights, Points ) #pass data to each funciton

                x_axis.append(x)
                y_axis.append(y)
            '''
            ############################################################################
            ############################################################################
            #  ENDBLOCK
            #
            ############################################################################
            '''
        return x_axis, y_axis

    #
    def plotFigure(self, saveTo=""):
        x_axis, y_axis = self.build_axis()

        m, b, r, p, e = linregress( x_axis, y_axis )
        #get boootstrapped error
        e1, e2 = self.helper.bs_err3(m, x_axis, y_axis, 1000)
        eqt = 'slope = ' + '{0:.3f}'.format(m) + '$\\pm$'+ '{:0.3f}'.format(abs(e1))

        #remove negative values
        for i in reversed( range( len(x_axis))):
            if x_axis[i] <= 0 or y_axis[i] <= 0:
                del x_axis[i]
                del y_axis[i]

        #get the error line
        line = [ m*xi + b for xi in x_axis]

        #fig = plt.figure()
        fig, ax = self.helper.custom_fig()

        #xlabel = '$\\mathregular{\\log{(m_{k,x} m_{k,y} m_{k,z})}}$'
        #ylabel = '$\\mathregular{\\log{(m_k)}}$'
        xlabel = 'log' + '$\\mathregular{(m_{k,x} m_{k,y} m_{k,z})}$'
        ylabel = 'log' + '$\\mathregular{(m_k)}$'

        plt.scatter( x_axis, y_axis )
        plt.plot( x_axis, line, color='blue', lw=self.lw , label=eqt)
        plt.plot( x_axis, x_axis, color='r', lw=self.lw)

        ax = plt.gca()
        x_max = ax.get_xlim()[1]
        y_max = ax.get_ylim()[1]
        ax.set_xlim([0, 301])
        ax.set_ylim([0, 301])
        ax.yaxis.set_ticks( np.arange(0, 301, 50))
        ax.xaxis.set_ticks( np.arange(0, 301, 50))

        #plt.legend(loc='upper left', fontsize=self.lfontsize)
        plt.legend(loc='upper left', fontsize=22)

        #plt.xlabel(xlabel, fontweight='bold', fontsize=self.fontsize)
        plt.xlabel(xlabel,  fontweight="normal",fontsize=26)
        plt.ylabel(ylabel, fontweight='normal', fontsize=26)

        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'separability scatter')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()


class SeparabilityHistogram( SeparabilityScatter ):

    def __init__(self):
        SeparabilityScatter.__init__(self)


    def build_axis(self):
        slopes = []

        #iterate through the files and build the axis points
        for fname in glob( './../data/*.cp.txt' ):
            fname_processed = fname.strip( '.cp.txt' ).split('/')[-1]

            #filter out the plants to exclude
            if not self.helper.filt( fname_processed, *self.filters ):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )
            x_axis = []
            y_axis = []
            for k in range(0, self.helper.KMax, 2 ):
                x, y = self.helper.xy_separability( k, weights, Points )
                x_axis.append(x)
                y_axis.append(y)

            m, b, r, p, e = linregress( x_axis, y_axis )
            slopes.append(m)

        return slopes

    def plotFigure(self, saveTo=''):

        slopes = self.build_axis()

        slope_mean = sum(slopes) / float(len(slopes))
        slope_dev = (sum([ (i - slope_mean) ** 2 for i in slopes ]) / float(len(slopes))) ** 0.5



        fig = self.helper.custom_fig()

        label = 'mean = ' + '{0:.3f}'.format(slope_mean) + '$\\pm$' + '{0:.3f}'.format(slope_dev)
        n, bins, patches = plt.hist(slopes, bins=20, range=(0.8, 1.2), histtype='stepfilled', label=label, color='b')
        plt.legend(loc='upper right', fontsize=18)
        plt.xlabel('Slope', fontsize=26, fontweight='normal')
        plt.ylabel('Frequency', fontsize=26, fontweight='normal')

        ax = plt.gca()
        #ax.set_ylim([0, 12])
        ax.set_xlim([0.8, 1.2])

        xmin, xmax = ax.get_xlim()
        ax.xaxis.set_ticks(np.arange(xmin, xmax, 0.1))
        #ax.yaxis.set_ticks(np.arange(0, 181, 20))
        plt.tight_layout()

        if saveTo:
            saveTo = './../figures/separability_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'separability histogram')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()
