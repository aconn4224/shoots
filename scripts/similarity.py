from figure import Figure
from glob import glob
from scipy.stats import linregress
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from math import log10
import math
import os
from random import randint
from collections import defaultdict



class ConvexHull( Figure ):
    def __init__(self):
        Figure.__init__(self)

    def build_axis(self, xnormal=True):

        x_axis = []
        y_axis = [] #to return

        convex_hulls = self.parse_convex_hull()
        for fname in glob( './../data/*.cp.txt' ):
            if not self.helper.filt(fname, *self.filters):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )


            fname = fname.strip( '.cp.txt' ).split('/')[-1]
            if 'data' in fname:
                fname = fname.split('\\')[-1]





            x, m0 = self.helper.xy_arbor_moment(0, weights, Points)

            if xnormal:
                x, m2 = self.helper.xy_arbor_moment(2, weights, Points)
                x, m0 = self.helper.xy_arbor_moment(0, weights, Points)
                m0 = 10**m0
                m2 = 10**m2
                x = math.sqrt(m2/m0)
                x = log10(x)



            try:
                y = convex_hulls[fname]
                x_axis.append(x)
                y_axis.append(log10(y))



            except:
                print("not in parse: " + fname)

        #remove negative values in x_axis
        for i in reversed(range(len(x_axis))):
            if x_axis[i] <= 0 or y_axis[i] <= 0:
                del x_axis[i]
                del y_axis[i]

        return x_axis, y_axis

    def plotFigure(self, xnormal=True, saveTo=''):

        x_axis, y_axis = self.build_axis(xnormal=xnormal)

        #get attributes of line
        m, b, r, p, std_err = linregress(x_axis, y_axis)
        e1, e2 = self.helper.bs_err3(m, x_axis, y_axis, 1000)
        e = max( [abs(e1), abs(e2)])


        r = '$R$ = ' + '{0:.3f}'.format(r) #legend string
        label = r

        line = [ (m * xi + b) for xi in x_axis] #line of best fit
        fig = self.helper.custom_fig()
        plt.scatter(x_axis, y_axis)
        plt.plot( x_axis, line, label=label, lw=self.lw, color='r' )
        plt.legend(loc='upper left', fontsize=22)

        ax = plt.gca()
        ax.xaxis.set_ticks(np.arange(0, 6, 1))
        ax.yaxis.set_ticks(np.arange(3, 7.5, .5))
        ax.set_xlim(-0.5, 5.5)
        ax.set_ylim(2.75, 7.0)

        if not xnormal:
            #plt.xlabel('$\\mathbf{\\log(m_{2} / m_{0})}$', fontsize=self.fontsize)
            plt.xlabel('log' + '$\\mathdefault{(m_{2} / m_{0})}$', fontsize=26, fontweight="normal")
        else:
            #plt.xlabel('$\\mathrm{\\log(\\sigma_{xyz} )}}$', fontsize=self.fontsize)
            plt.xlabel('log' + '$\\mathdefault{(\\sigma_{xyz} )}$', fontsize=26, fontweight="normal")
            plt.ylabel('log' + '$\\mathdefault{(convex\ hull\ volume)}$', fontsize=26, fontweight="normal")
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/similarity_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'convex hull vs m_2')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:

            plt.show()



    #returns a dictionary with the label as the plant and day and the value as the
    # convex hull. Example, dict['m82D_control_A_D00'] = 56 mm^3
    def parse_convex_hull( self ):

        #create path to csv files
        csv_direct = './../csv/'
        with open( os.path.join( csv_direct , 'convex_hulls.csv' )) as f:

            #structure to store convex hulls
            convex_hulls = {}
            for line in f:
                if line.startswith('#end'): break #end of file
                if not line.rstrip(): continue
                label, value = line.rstrip().split(',')

                #skip the first line in file and blank lines
                if label == 'label' or len(label) == 0:
                    continue

                #label should be control_A_D00 for example
                convex_hulls[label] = float(value)
        return convex_hulls


class SimilarityScatter( Figure ):
    def __init__(self):
        Figure.__init__(self)

    def build_axis(self, kstep=2):
        Axis = {}


        #initiate Axis
        Axis = { k: ([],[]) for k in range(0, self.helper.KMax, kstep) }

        for fname in glob( './../data/*.cp.txt' ):

            if not self.helper.filt( fname, *self.filters):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate)

            for k in Axis:
                x, y = self.helper.xy_arbor_moment(k, weights, Points )
                x, m0 = self.helper.xy_arbor_moment(0, weights, Points )
                x, m2 = self.helper.xy_arbor_moment(2, weights, Points )

                #normalie xval
                m0 = 10**m0
                m2 = 10**m2

                x = math.sqrt(m2/m0)
                x = log10(x)

                #normalize y val
                trash, y0 = self.helper.xy_arbor_moment(0, weights, Points)
                y = y - y0
                Axis[k][0].append(x)
                Axis[k][1].append(y)

        return Axis

    def plotFigure(self, kstep=2, saveTo=""):

        Axis = self.build_axis( kstep=kstep)


        colors = cm.rainbow(np.linspace(0, 1, len(Axis)))
        ck = -1
        fig = self.helper.custom_fig()

        for k in sorted(Axis.keys()):
            ck += 1
            x_axis, y_axis = Axis[k]
            m, b, r, p, std_err = linregress(x_axis, y_axis)

            #remove zero values from axis
            for i in reversed( range(len(x_axis))):
                if x_axis[i] < 0 or y_axis[i] < 0:
                    del x_axis[i]
                    del y_axis[i]

            line = [ (m*xi + b) for xi in x_axis]

            plt.scatter( x_axis, y_axis, color=colors[ck], label='k=' + str(k))
            plt.plot( x_axis, line, color=colors[ck], lw=self.lw )


        plt.xlabel('log' + '$\\mathregular{(\\sigma_{xyz})}$', fontsize=26, fontweight="normal")
        plt.ylabel('log' + '$\\mathregular{(\\frac{m_k}{m_0})}$', fontsize=26, fontweight="normal")
        #plt.xlabel('$\\mathregular{\\log{(\\sigma_{xyz})}}$', fontsize=self.fontsize, fontweight=self.fontweight)
        #plt.ylabel('$\\mathregular{\\log{(\\frac{m_k}{m_0})}}$', fontsize=self.fontsize, fontweight=self.fontweight)
        ax = plt.gca()
        ax.set_xlim([-.5, 12])
        ax.set_ylim([-10, 250])
        ax.xaxis.set_ticks(np.arange(0, 13, 2))
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/similarity_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'colored moments')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()






class SimilaritySlopes( SimilarityScatter ):
    def __init__(self):
        SimilarityScatter.__init__(self)

    def build_axis(self, iterations=1000, kstep=2):
        Axis = super( SimilaritySlopes, self).build_axis(kstep=kstep)

        ks = []
        slopes = []
        m_errors = []
        for k in sorted(Axis.keys()):
            ks.append(k)
            x_axis, y_axis = Axis[k]
            m, b, r, p, e = linregress( x_axis, y_axis)
            slopes.append( m )

            m_error = []
            for i in range(iterations):
                new_i = [randint(0, len(x_axis) - 1) for i in range(len(x_axis))]
                new_x = [x_axis[i] for i in new_i]
                new_y = [y_axis[i] for i in new_i]

                m1, b1, r, p, e = linregress(new_x, new_y)

                m_error.append( m - m1)

            # part 1 and 2
            loweri = 0
            upperi = len(m_error) - 1

            m_error = sorted(m_error)
            lower = m_error[loweri]
            upper = m_error[upperi]
            m_error = max(abs(lower), abs(upper))
            m_errors.append(m_error)
        return ks, slopes, m_errors

    def plotFigure(self, iterations=1000, kstep=2, saveTo=''):

        ks, slopes, m_errors = self.build_axis(iterations=iterations, kstep=kstep)
        m, b, r, p, m_error = linregress(ks, slopes)

        fig = self.helper.custom_fig()

        #bootstrap error is in equation
        eqt = 'slope = ' + '{0:.3f}'.format(m) + '$\pm$' + '{:.3f}'.format(m_error)

        #errorbars come from std error
        plt.errorbar(ks, slopes, yerr=m_errors, marker='o', c='#f44e42', markeredgecolor='#f44e42', mew=2, ms=12, markerfacecolor='none',  lw=3, label=eqt)
        plt.legend( loc='upper left', fontsize=22, numpoints=1)
        plt.xlabel('Moment order (k)', fontsize=26)
        plt.ylabel('Slope', fontsize=26)

        ax = plt.gca()
        start, endx = ax.get_xlim()
        starty, endy = ax.get_ylim()
        end = max(endx, endy)

        ax.xaxis.set_ticks(np.arange(0, 21, 5))
        ax.yaxis.set_ticks(np.arange(0, 21 , 5))
        ax.set_xlim(-.5, 21)
        ax.set_ylim(-.5, 21)

        #ax.set_xlim( [0, 20])
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/similarity_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'slopes vs k similarity')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()







class SimilarityIntercepts( SimilarityScatter ):
    def __init__(self):
        SimilarityScatter.__init__(self)

    def build_axis(self, iterations=1000, kstep=2):
        Axis = super( SimilarityIntercepts, self).build_axis(kstep=kstep)

        ks = []
        intercepts = []
        b_errors = []
        for k in sorted(Axis.keys()):

            ks.append(k)
            x_axis, y_axis = Axis[k]
            m, b, r, p, e = linregress( x_axis, y_axis)
            intercepts.append( b )

            b_error = []
            for i in range(iterations):
                new_i = [randint(0, len(x_axis) - 1) for i in range(len(x_axis))]
                new_x = [x_axis[i] for i in new_i]
                new_y = [y_axis[i] for i in new_i]

                m1, b1, r, p, e = linregress(new_x, new_y)

                b_error.append( b - b1)

            # part 1 and 2
            loweri = 0
            upperi = len(b_error) - 1

            b_error = sorted(b_error)
            lower = b_error[loweri]
            upper = b_error[upperi]
            b_error = max(abs(lower), abs(upper))
            b_errors.append(b_error)
        return ks, intercepts, b_errors

    def plotFigure(self, iterations=1000, saveTo="", kstep=2, distributions=[]):

        ks, intercepts, b_errors = self.build_axis( iterations=iterations, kstep=kstep)
        m, b, r, p, std_err = linregress(ks, intercepts)

        fig = self.helper.custom_fig()

        yg = self._getGaussians( )

        #plt.errorbar(ks, intercepts, yerr=b_errors, marker='o', color='red', lw=3, label="Plants: ")
        #plt.plot(yg['x'],yg['yg14'],lw=3,c="black", linestyle='--',label="Gaussian 1.4")
        #plt.plot(yg['x'],yg['yg24'],lw=3,c="black", linestyle='--', label="Gaussian 2.4")
        #plt.plot(x,yg26,lw=3,c="black", linestyle='--', label="Gaussian 2.6")
        plt.plot(yg['x'],yg['yg34'],lw=3,c="black", linestyle='--',  label="Gaussian 3.4")
        plt.plot(yg['x'],yg['yg24'],lw=3,c="black", linestyle='-',label="Gaussian 2.4")

        plt.errorbar(ks, intercepts, yerr=b_errors, marker='o', c='#f44e42', lw=3, label="Plants ", markeredgecolor='#f44e42', mew=2, ms=12, markerfacecolor='none')
        plt.plot(yg['x'],yg['yp'],lw=3,c="black", linestyle='-.',  label="Uniform")

        plt.legend(loc="upper right", fontsize=12)
        plt.xlabel('Moment order (k)', fontsize=26)
        plt.ylabel('Intercept', fontsize=26)

        ax = plt.gca()

        handles, labels = ax.get_legend_handles_labels()
        ax.legend( [handles[0],handles[1], handles[3], handles[2]],  [labels[0],labels[1], labels[3], labels[2]], loc='upper left', fontsize=22, numpoints=1)

        start, endx = ax.get_xlim()
        starty, endy = ax.get_ylim()
        end = max(endx, endy)

        ax.xaxis.set_ticks(np.arange(0, 21, 5))
        ax.yaxis.set_ticks(np.arange(0, 21 , 5))
        ax.set_xlim(-1, 21)
        ax.set_ylim(-1, 21)

        #ax.set_xlim( [0, 20])
        plt.tight_layout()
        if saveTo:
            saveTo = './../figures/similarity_figs/' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'slopes vs k similarity')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()


    def _getGaussians( self ):

        yg = {}
        '''Sakets plots for different distributions'''
        add = lambda q: q

        # plant.
        yg['x'] = [0,2,4,6,8,10,12,14,16,18,20]

        #pillbox.
        yg['yp'] = list(map(add,[0.0,0.0,0.597317,1.36065,2.20276,3.09106,4.00975,4.94996,5.90621,6.87489,7.85348]))

        #gaussian 1.4
        yg['yg14'] = list(map(add,[0.0,0.0,0.670731,1.52291,2.45979,3.4457,4.46358,5.50395,6.56099,7.6309,8.71103]))

        #gaussian 1.6
        yg['yg16'] = list(map(add,[0.0,0.0,0.696,1.578,2.548,3.567,4.619,5.693,6.785,7.889,9.004]))

        #gaussian 2.0
        yg['yg20'] = list(map(add,[0.0,0.0,0.760,1.721,2.775,3.881,5.021,6.185,7.366,8.560,9.765]))
        #gaussian 2.4
        yg['yg24'] = list(map(add,[0.0,0.0,0.844311,1.91191,3.07934,4.30278,5.56195,6.84586,8.14788,9.46375,10.7905]))

        #gaussian 2.6
        yg['yg26'] = list(map(add,[0.0,0.0,0.894,2.026,3.261,4.556,5.887,7.244,8.619,10.009,11.409]))

        #gaussian 3.0
        yg['yg30'] = list(map(add,[0.0,0.0,1.006,2.284,3.681,5.141,6.641,8.168,9.715,11.276,12.849]))

        #gaussian 3.4
        yg['yg34'] = list(map(add,[0.0,0.0,1.12437,2.56915,4.14974,5.80229,7.49877,9.22468,10.9716,12.7344,14.5094]))

        #gaussian 3.6
        yg['yg36'] = list(map(add,[0.0,0.0,1.182,2.712,4.391,6.146,7.948,9.781,11.635,13.506,15.390]))

        return yg
