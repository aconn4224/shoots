import numpy as np
from glob import glob
from math import sqrt, log10, factorial
import matplotlib.pyplot as plt
from scipy.stats import linregress
from figure import Figure




class VolumeVersusLengthScatter( Figure ):

    def __int__(self):
        Figure.__init__(self)


    def build_axis( self, plot_by_env=False, moment=0):

        #make the marks by environment or species
        x_axis = []
        y_axis = []

        Axis = {}

        for fname in glob( './../data/*.cp.txt' ):
            if not self.helper.filt(fname, *self.filters):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )

            fname = fname.strip( '.cp.txt' ).split('/')[-1]
            if 'data' in fname:
                fname = fname.split('\\')[-1]



            if plot_by_env:
                env = fname.split('_')[1] # --> m82D_control_A_D00 --> control
            else:
                env = fname.split('_')[0] # --> m82D_control_A_D00 --> m82D

            x, y = self.helper.xy_arbor_moment(moment, weights, Points)
            x, m0 = self.helper.xy_arbor_moment(0, weights, Points)
            x, m2 = self.helper.xy_arbor_moment(2, weights, Points)

            m0 = 10**m0
            m2 = 10**m2
            x = sqrt(m2 / m0)
            x = log10(x)


            if 'control' in env:
                env='control'

            #add them to list to color
            if env not in Axis.keys():
                Axis[env] = ([],[])
            Axis[env][0].append(x)
            Axis[env][1].append(y) #append to Axis so they can be colored according to environment

        return Axis


    def plotFigure(self, plot_by_env=False, moment=0, saveTo=""):


        Axis = self.build_axis( plot_by_env=plot_by_env, moment=moment)

        #get parameters to configure graph
        params = {}

        red =    '#E24A33'
        blue =   '#348ABD'
        violet = '#988Ed5'
        grey =   '#777777'
        yellow = '#FBC15E'
        green =  '#8EBA42'
        pink =   '#FF85B8'
        black =  '#000000'
        purple = '#990099'
        green2 = '#006600'
        brown =  '#ff6600'


        params['control']   = ['Control ', green, 'o']
        params['control1']   = ['Control ', green, 'o']
        params['control2']   = ['Control ', green, 'o']
        params['shade']     = ['Shade ',blue, 'o']
        params['heat']      = ['High-heat ', red, 'o']
        params['highlight'] = ['High-light ', pink, 'o']
        params['drought']   = ['Drought ', yellow, 'o']

        params['m82D'] = ['Tomato', green2, 'o']
        params['benthi'] = ['Tobacco', purple, 'o']
        params['sorghum'] = ['Sorghum', brown, 'o']

        fig = self.helper.custom_fig()

        all_x = []
        all_y = []

        if plot_by_env:
            keys = ['shade', 'control', 'highlight', 'drought', 'heat']
        else:
            keys = ['sorghum', 'm82D', 'benthi']


        for env in sorted(keys):

            xy = Axis[env]

            label = params[env][0]
            color = params[env][1]
            mark  = params[env][2]

            all_x += xy[0]
            all_y += xy[1]
            plt.scatter( xy[0], xy[1], color=color, marker=mark, label=label , s=50)


        m, b, r, p, e = linregress(all_x, all_y )
        y_line = [ (m*xi + b) for xi in all_x]

        plt.plot( all_x, y_line, color='black', lw=2)
        plt.legend(loc='upper left', fontsize=18)

        #plt.xlabel('$\\mathregular{\\log{(Volume)}}$', fontsize=24, fontweight="normal")
        plt.xlabel('log(Volume)', fontsize=26, fontweight="normal")
        if moment == 0:
            plt.ylabel('log(Length)', fontweight="normal", fontsize=26)
        elif moment == 10:
            plt.ylabel( "log(" +  '$\\mathregular{m_{10})}$', fontweight="normal", fontsize=self.fontsize)
        elif moment == 20:
            plt.ylabel('$\\mathregular{\\log{(m_{20})}}$', fontweight="normal", fontsize=self.fontsize)


        ax = plt.gca()
        ax.set_xlim([2, 12])
        if moment == 0:
            ax.set_ylim([2, 5.5])
            ax.yaxis.set_ticks(np.arange(2, 6, .5))

        plt.tight_layout()

        if saveTo:
            if plot_by_env:
                saveTo = './../figures/dist_to_regression/env_' + saveTo
            else:
                saveTo = './../figures/dist_to_regression/species_' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'mk versus m2')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()


class DistanceToRegressionHist( Figure ):
    def __init__(self):
        Figure.__init__(self)


    #this function parses the
    def build_regression( self, moment=0 ):

        #make the marks by environment or species
        x_axis = []
        y_axis = []

        for fname in glob( './../data/*.cp.txt' ):
            if not self.helper.filt(fname, *['','','8']):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )

            fname = fname.split('/')[-1]

            env = fname.split('_')[1] # --> m82D_control_A_D00 --> control


            x, y = self.helper.xy_arbor_moment(moment, weights, Points)
            x, m0 = self.helper.xy_arbor_moment(0, weights, Points)
            x, m2 = self.helper.xy_arbor_moment(2, weights, Points)

            m0 = 10**m0
            m2 = 10**m2
            x = sqrt(m2 / m0)
            x = log10(x)


            if 'control' in env:
                env='control'


            x_axis.append(x)
            y_axis.append(y) #append to Axis so they can be colored according to environment

        m, b, r, p, e = linregress( x_axis, y_axis )
        return m, b

    #return a dict with Axis['m82D']['control'] --> bins
    def build_axis( self, moment=0 ):
        m, b = self.build_regression( moment=moment)

        #make the marks by environment or species
        Axis = {}
        species = ['m82D', 'benthi', 'sorghum']
        conditions = ['control', 'shade', 'heat', 'highlight', 'drought']

        #initiate Axis
        for s in species:
            Axis[s] = {}
            for c in conditions:
                Axis[s][c] = []

        for s in species:

            for c in conditions:
                filters = [s, c, '8']
                for fname in glob( './../data/*.cp.txt'):
                    if self.helper.filt( fname, *filters):

                        weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate )

                        x, y = self.helper.xy_arbor_moment(moment, weights, Points)
                        x, m0 = self.helper.xy_arbor_moment(0, weights, Points)
                        x, m2 = self.helper.xy_arbor_moment(2, weights, Points)

                        m0 = 10**m0
                        m2 = 10**m2
                        x = sqrt(m2 / m0)
                        x = log10(x)

                        Axis[s][c].append( y - (m * x + b))
                        #x_axis.append(x)
                        #y_axis.append(y) #append to Axis so they can be colored according to environment



        return Axis

    def plotFigure( self, moment=0, species='', condition='', saveTo=""):

        if not species or not condition:
            self.plot_all(moment=moment, saveTo=saveTo)
            return

        Axis = self.build_axis( moment=moment)
        vals = Axis[species][condition]


        percent = [ 0 if x < 0 else 1 for x in vals ] #get number above the expected line

        p_val = self.get_Pval( 0.5, percent ) #get the p_val of the percentage

        if sum(percent) < 0.5 * len(percent):
            p_val = 1 - p_val   #change the p_value to match the symetrical case

        percent = 100 * (float( sum( percent)) / len( percent) )
        label = '%' + '{0:.1f}'.format(percent) + ' (p=' + '{:.2e}'.format(p_val) + ')'

        fig = self.helper.custom_fig()
        if moment == 0:
            bins = np.arange( -.3, .35 + .05 + 0.05, 0.05)
            plt.hist(vals, bins=bins, color='b')
        else:
            plt.xlim([-1.5, 1.5])
            plt.hist(vals, color='b')

        plt.plot((0,0), (0,20), color='r', lw=4)

        ax = plt.gca()
        if moment == 0:
            plt.ylim([0,15])
            plt.xlim([-.42, .4])
            ax.xaxis.set_ticks(np.arange(-.4, .41, .1))
            ax.yaxis.set_ticks(np.arange(0, 15, 2))
        else:
            plt.ylim([0, 15])
            plt.xlim([-1.5, 1.5])
        label = '%' + '{0:.1f}'.format(percent) + ' p=' + '{:.2e}'.format(p_val) + ''
        title = species + '_' + condition + '_above8_' + label + '.pdf'
        plt.xlabel(r'$\mathregular{Distance\ to\ regression\ line}}}$', fontsize=26, fontweight='regular')
        plt.ylabel(r'$\mathregular{\#\ of\ plants}}$', fontsize=26, fontweight='regular') #add x label
        plt.tight_layout()
        #plt.savefig( './../figures/final/distance_to_histogram/' + title)
        if saveTo:
            saveTo = saveTo.strip('.pdf') + label + '_m' + str(moment) + '_.pdf'

            saveTo = './../figures/dist_to_regression/' + saveTo

            try:
                plt.savefig(saveTo)
                self.log_parameters( 'distance to regression')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()


    def plot_all(self, moment=0, saveTo=""):
        Axis = self.build_axis(moment=moment)


        fig, axes = plt.subplots( nrows=4, ncols=3,sharex=True, sharey=True )
        species = ['m82D', 'benthi', 'sorghum']
        conditions= ['control', 'shade','heat','highlight', 'drought']


        n = -1
        for s in species:
            for c in conditions:
                if not len(Axis[s][c]):
                    continue

                dist = Axis[s][c]
                bins = np.arange( -.3, .35 + .05 + 0.05, 0.05)
                bins = np.arange( -.3, .35 + .05 + 0.05, 0.05)
                n += 1
                i = int(n / 3)
                j = n % 3  #get the i, j indeces of axes



                percent = [ 0 if x < 0 else 1 for x in dist ] #get number above the expected line

                p_val = self.get_Pval( 0.5, percent ) #get the p_val of the percentage

                if sum(percent) < 0.5 * len(percent):
                    p_val = 1 - p_val   #change the p_value to match the symetrical case

                percent = 100 * (float( sum( percent)) / len( percent) )
                label = '%' + '{0:.1f}'.format(percent) + ' (p=' + '{:.2e}'.format(p_val) + ')'

                #axes[i][j].hist(dist, bins=bins )
                axes[i][j].hist(dist )
                axes[i][j].plot( (0,0), (0, 20), color='r', lw=4, label=label)
                axes[i][j].legend( loc='upper right', fontsize=8)
                #axes[i][j].xlabel(r'$\mathrm{(Distance\ to\ regression\ line)}}}$', fontsize=30, fontweight='bold')
                #axes[i][j].ylabel(r'$\mathrm{Number\ of\ plants}}$', fontsize=30) #add x label
                axes[i][j].set_title( s + '/' + c, fontsize=8 )


        if saveTo:

            saveTo = "./../figures/dist_to_regression/" + 'm_' + str(moment) + '_' + saveTo
            try:
                plt.savefig(saveTo)
                self.log_parameters( 'distance to regression')
            except:
                raise ValueError( "Enter rel path and file name to where file should save")
        else:
            plt.show()

    def get_Pval(self, p, data ):

        #data = [0, 0, 0, 1, 1, 1, 1]
        n = len(data)   #get length
        k = sum( data ) #get number of 1s
        p_val = sum( [self.nChooseK(n, i)*(p**n) for i in range( k, n+1)])

        return p_val



    def nChooseK(self, n, k ):

        if k > n or k < 0 or n == 0:
            raise ValueError( 'k must be between n and 0')
        f = factorial
        return f(n) / f(k) / f(n-k)
