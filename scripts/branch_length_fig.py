from scipy.optimize import curve_fit
from scipy.stats import kstest
from figure import *


class BranchPointLength( Figure ):
    def __init__(self):
        Figure.__init__(self)

    def build_axis( self ):
        initialSegment = self
        self.helper.segment = 1
        self.helper.scale = 1

        lengths = []

        for fname in glob( './../data/*.cp.txt'):
            if 'sorghum' in fname:
                continue
            if not self.helper.filt(fname, *self.filters):
                continue

            weights, Points = self.helper.parse_cp( fname, rotate=self.helper.rotate)
            lengths += list(weights)

        if len(lengths) == 0:
            return 0
        return lengths

    def plotFigure(self):
        lengths = self.build_axis()
        fig = self.helper.custom_fig()
        bins = np.arange(0, 45, 2)
        hist = np.histogram(lengths, bins)

        plt.hist( lengths, bins, color='b')

        #build exponential line
        x = np.array(hist[1][1:], dtype=float)
        y = np.array(hist[0], dtype=float)

        popt, pcov = curve_fit(self.func, x, y, [100, 400, .001, 0])


        y = self.func(hist[1][1:], *popt)

        d, p = kstest( y, 'expon')


        m, b, r, p, e  = linregress( hist[0], y)


        if p < .001:
            label = 'P < 0.0001'
        else:
            label = 'P=' + '{:.2f}'.format( float(p))

        x = np.arange(0, 44, .1)
        plt.plot( x, func(x, *popt), color='r', lw=self.lw, label='P < 0.0001' )
        plt.legend(loc="upper right", fontsize=22)


        #plt.legend( loc='upper right', fontsize=15)
        plt.xlabel(r'$\mathregular{Branch\ length\ (mm)}$', fontsize=26, fontweight='normal')
        plt.ylabel(r'$\mathregular{\#\ of\ branches}}$', fontsize=26, fontweight="normal") #add x label

        plt.ylim([0,1550])

        title = '_'.join(self.filters) + '.pdf'
        plt.tight_layout()

        plt.show()

    def func(self, x, a, b, c, d):
        return a*np.exp( -c*(x-b)) + d





def graph_branchlength_hist(filters=[], rotate=False):


    fig = custom_fig()
    bins = np.arange(0, 45, 2)
    hist = np.histogram(l, bins)



    plt.hist( l, bins=bins, color='b')



    #build exponential line
    x = np.array(hist[1][1:], dtype=float)
    y = np.array(hist[0], dtype=float)


    popt, pcov = curve_fit(func, x, y, [100, 400, .001, 0])


    y = func(hist[1][1:], *popt)

    d, p = kstest( y, 'expon')


    m, b, r, p, e  = linregress( hist[0], y)


    if p < .001:
        label = 'P < .0001'
    else:
        label = 'P=' + '{:.2f}'.format( float(p))

    x = np.arange(0, 44, .1)
    plt.plot( x, func(x, *popt), color='r', lw=lw, label='P < .0001' )
    plt.legend(loc="upper right", fontsize=30)


    #plt.legend( loc='upper right', fontsize=15)
    plt.xlabel(r'$\mathrm{Branch\ length (mm)}$', fontsize=30, fontweight='bold')
    plt.ylabel(r'$\mathrm{\#\ of\ Branches}}$', fontsize=30) #add x label

    plt.ylim([0,1550])

    title = '_'.join(filters) + '.pdf'
    plt.tight_layout()

    plt.savefig( './../figures/rotated_figures/branch_length/' + title)
    #plt.show()

def func(x, a, b, c, d):
    return a*np.exp( -c*(x-b)) + d
