
from figure import Figure
from separability import SeparabilityScatter, SeparabilityHistogram
from similarity import *
from distance_to_regression_figs import *
from figure_modifications import *
from branch_length_fig import *

rebuild = False

#FIGURE 2A
myfig = SeparabilityScatter()
myfig.update_parameters( scale=1000, segments=10, KMax=20, rotate=True )
#uncomment to plot
#myfig.plotFigure()



#FIGURE 2B
myfig = SeparabilityHistogram(  )
myfig.update_parameters( scale=1000, segments=10, KMax=20, rotate=True )
#uncomment to plot
#myfig.plotFigure()


#FIGURE 3A
myfig = ConvexHull( )
myfig.update_parameters( scale=1, segments=10, KMax=21, rotate=True )
#uncomment to plot
#myfig.plotFigure()


#FIGURE 3B
myfig = SimilarityScatter()
myfig.update_parameters( scale=100, segments=10, KMax=21 )
#uncomment to plot
#myfig.plotFigure(kstep=2)


#FIGURE 3C
myfig = SimilaritySlopes()
myfig.update_parameters( scale=1, segments=10, KMax=21 )
#uncomment to plot
#myfig.plotFigure(iterations=100)


#FIGURE 4
myfig = SimilarityIntercepts()
#uncomment correct filter
f = ['','',''] #graph all
# f = ['sorghum', '', '']
# f = ['m82D', '', '']   #m82D is tomato genotype
# f = ['benthi', '', ''] #benthi is tobacco
myfig.update_parameters( scale=1, segments=10, KMax=21, rotate=True, filters =f )
#uncomment to plot - truncated gaussians need to be updated in separability.py: Class SimilarityIntercepts --> plotFigure()
#myfig.plotFigure()


#FIGURE 5
myfig = VolumeVersusLengthScatter()
myfig.update_parameters( scale=100, segments=10, KMax=21, filters=['','','8'] )
#uncomment for figure 5A
#myfig.plotFigure( plot_by_env=False, moment=0)
#uncomment for figure 5B
#myfig.plotFigure( plot_by_env=True, moment=0)

#FIGURE 5 C-E
myfig = DistanceToRegressionHist( )
myfig.update_parameters( scale=100, segments=10, KMax=21, rotate=True )
#uncomment to plot with appropriate keyword args
#myfig.plotFigure(moment=0, species='sorghum', condition='control', saveTo="sorghum_control.pdf")
#myfig.plotFigure(moment=0, species='m82D', condition='drought', saveTo="m82D_drought.pdf")
#myfig.plotFigure(moment=0, species='benthi', condition='heat', saveTo="benthi_heat.pdf")

'''
############################################################################
############################################################################
#  SUPPLEMENTARY FIGURES
#
############################################################################
'''

#SUPPLEMENTARY
myfig = OddEvenSimilarityScatter()
myfig.update_parameters( scale=100, segments=10, KMax=21 )
#myfig.plotFigure(kstep=1)

#SUPPLEMENTARY
myfig = OddEvenSimilaritySlopes()
myfig.update_parameters( scale=1, segments=10, KMax=21 )
#myfig.plotFigure(kstep=1)


#SUPPLEMENTARY
myfig = OddEvenSimilarityIntercepts()
myfig.update_parameters( scale=100, segments=10, KMax=21 )
#myfig.plotFigure(kstep=1)

#SUPPLEMENTARY
myfig = BranchPointLength()
myfig.update_parameters( scale=1, segments=1, KMax=20)
myfig.build_axis()
#myfig.plotFigure()



#The following graphs similarity scatter by conditions and then by species (not pairwise)
#Set to True to graph separability scatter for by each condition/species
if False:

    species = ['m82D', 'benthi', 'sorghum']
    envs    = ['control', 'shade', 'heat', 'highlight', 'drought']

    for specie in species:
        filters = [specie, '', '']
        pdftitle = "similarity_scatter_{}.pdf".format(specie)
        myfig = SimilarityScatter()
        myfig.update_parameters( scale=100, segments=10, KMax=21, filters=filters )
        #myfig.plotFigure(kstep=2, saveTo=pdftitle)

    for env in envs:
        filters = ['', env, '']
        pdftitle = "similarity_scatter_{}.pdf".format(env)
        myfig = SimilarityScatter()
        myfig.update_parameters( scale=100, segments=10, KMax=21, filters=filters )
        #myfig.plotFigure(kstep=2, saveTo=pdftitle)



#Set to True to graph separability slopes for by each condition/species
if False:
    species = ['m82D', 'benthi', 'sorghum']
    envs    = ['control', 'shade', 'heat', 'highlight', 'drought']

    for specie in species:
        filters = [specie, '', '']
        pdftitle = "similarity_slopes_{}.pdf".format(specie)
        myfig = SimilaritySlopes()
        myfig.update_parameters( scale=1, segments=10, KMax=21, filters=filters )

        #myfig.plotFigure()
        myfig.plotFigure(kstep=2, saveTo=pdftitle)

    for env in envs:
        filters = ['', env, '']
        pdftitle = "similarity_slopes_{}.pdf".format(env)
        myfig = SimilaritySlopes()
        myfig.update_parameters( scale=1, segments=10, KMax=21, filters=filters )
        myfig.plotFigure(kstep=2, saveTo=pdftitle)
        #print(pdftitle)
        #myfig.plotFigure()
